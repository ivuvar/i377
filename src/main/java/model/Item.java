package model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
@Table(name = "order_rows")
public class Item {

    @NotNull
    @Column(name = "item_name")
    private String itemName;

    @NotNull
    @Range(min = 1)
    private Integer price;

    @NotNull
    @Range(min = 1)
    private Integer quantity;
}
