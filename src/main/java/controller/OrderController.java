package controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import model.Order;
import dao.OrderDao;

import javax.validation.Valid;
import java.util.List;

@RestController
public class OrderController {

    @Autowired
    private OrderDao orderDao;

    @GetMapping(value = "orders", produces = MediaType.APPLICATION_JSON_VALUE)
    protected List<Order> getOrders() {
        return orderDao.getAll();
    }

    @GetMapping(value = "/orders/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    protected Order getOrderById(@PathVariable Long id) {
        return orderDao.getById(id);
    }

    @PostMapping(value = "/orders", produces = MediaType.APPLICATION_JSON_VALUE)
    protected Order postOrder(@RequestBody @Valid Order order) {
        orderDao.save(order);
        return order;
    }

    @DeleteMapping("/orders/{id}")
    protected void deleteOrderById(@PathVariable Long id) {
        orderDao.delete(id);
    }

    @DeleteMapping("/orders")
    protected void deleteOrders() {
        orderDao.deleteAll();
    }
}
